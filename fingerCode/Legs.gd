extends Sprite

signal you_have_legs

var rammasse = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	print(body.get_name())
	if(body.get_name() == "Player"):
		rammasse = 1
		emit_signal("you_have_legs")
		self.hide()
	else:
		self.position.x += 50;
	


func _on_Player_drop_legs(x_player, y_player):
	if(rammasse):
		rammasse = 0
		self.position.x = x_player - 50
		self.position.y = y_player
		self.show()


func _on_Appearing_platform_move():
	self.position.x += 50
