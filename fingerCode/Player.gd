extends KinematicBody2D

# Declare member variables here. Examples:
var celerity = Vector2(0,0)
var speed = 100
var hauteur = 50;

onready var collision = get_node("Collision")
var shape

var arms = 0
var legs = 0
var head = 0

signal drop_legs
signal drop_arms

# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	shape = collision.get_shape()
	
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#celerity = Vector2(0,0)
	self.get_input()
	#print(shape.get_extents().y)
	pass


func get_input():
	if(Input.is_action_pressed("ui_right")):
		celerity.x = speed
	elif(Input.is_action_pressed("ui_left")):
		celerity.x = -speed
	else:
		celerity.x = 0
	#if(Input.is_action_pressed("ui_down")):
	
	if(is_on_floor()):
		celerity.y = 0
		if(Input.is_action_pressed("ui_up")  and legs):
			celerity.y = -6*speed + 3*arms*speed
	else:
		celerity.y += speed/4
		
	if(Input.is_action_just_pressed("drop_legs")):
		_drop_legs()
	
	if(Input.is_action_just_pressed("drop_arms")):
		_drop_arms()
		
	
	move_and_slide(celerity, Vector2(0, -1))

func _on_Head_you_have_a_head():
	if(head != 1):
		head = 1
		hauteur += 5
		shape.set_extents(Vector2(16,hauteur))
		print("Yeah a head\n")



func _on_Arms_you_have_arms():
	if(arms != 1):
		arms = 1
		print("Yeah arms\n")

func _on_Legs_you_have_legs():
	if(legs != 1):
		legs = 1
		hauteur += 10
		shape.set_extents(Vector2(16,hauteur))
		print("Yeah legs\n")


func _drop_legs():
	if(legs != 0):
		legs = 0
		hauteur -= 10
		shape.set_extents(Vector2(16,hauteur))
		print("Stay there\n")
		emit_signal("drop_legs", self.position.x, self.position.y)
	else:
		print("I don't have legs\n")

func _drop_arms():
	if(arms !=0):
		arms = 0
		print("Don't harm anyone\n")
		emit_signal("drop_arms", self.position.x, self.position.y)
	else:
		print("No arms, no chocolate\n")
	
func _drop_head():
	if(head != 0):
		head = 0
		hauteur -= 5
		shape.set_extents(Vector2(16,hauteur))
		print("Hope it will not roll away\n")
	else:
		print("Looks like you don't have a brain too\n")
	

