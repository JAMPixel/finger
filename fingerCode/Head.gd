extends Sprite

signal you_have_a_head

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	emit_signal("you_have_a_head")
	if(Global.lvl == 0):
		Global.goto_scene("res://Level1.tscn")
	self.hide()
