extends AnimatedSprite

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player;

# Called when the node enters the scene tree for the first time.
func _ready():
	player = self.get_parent()
	self.stop()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#if((self.get_parent().celerity as Vector2).length() != 0):
	if(player.celerity.x != 0):
		if(player.head and player.arms and player.legs):
			self.play("Walk_all")
		elif(player.head and player.arms):
			self.play("Walk_head_arms")
		elif(player.head and player.legs):
			self.play("Walk_head_legs")
		elif(player.legs and player.arms):
			self.play("Walk_arms_legs")
		elif(player.head):
			self.play("Walk_with_head")
		elif(player.legs):
			self.play("Walk_with_legs")
		elif(player.arms):
			self.play("Walk_arms")
		else:
			self.play("Walk_Stick")
			
	else:
		self.stop()
		#self.play("Static")
	pass
